import pytest
import cpp
from sismic.io import import_from_yaml

def statechart1():
        yaml = """statechart:
  name: example
  root state:
    name: root
"""
        return yaml

def statechart2():
        yaml = """statechart:
  name: example
  root state:
    name: root
    states:
      - name: s1
        transitions:
        - target : s2
          action: |
            #my_function();
      - name: s2
        transitions:
          - target: s1
            action: |
             #This is an action
"""
        return yaml


def statechart3():
        yaml = """statechart:
  name: example
  root state:
    name: root
    states:
      - name: s1
        transitions:
        - target : s2
          action: |
            #my_function();
      - name: s2
        transitions:
          - target: s1
            action: |
             #my_function() && something;
"""
        return yaml


def statechart4():
        yaml = """statechart:
  name: example
  root state:
    name: root
    states:
      - name: s1
        transitions:
        - target : s1
          action: |
            #my_function();
            #my_function2() && something;
"""
        return yaml

def statechart5():
        yaml = """statechart:
  name: example
  root state:
    name: root
    states:
      - name: s1
        transitions:
        - target : s1
          action: |
            #my_function(something,5,22);
"""
        return yaml

def yaml2cppexporter(yaml):
        return cpp.CppExporter(import_from_yaml(yaml)) 

def test_export_function_with_no_function():
        exporter = yaml2cppexporter(statechart1())
        exporter.export_function()
        assert len(exporter._output)/3 == 1 # the show function
        assert exporter._output[0] == 'void ' + 'show()'
        assert exporter._output[1] == '{'
        assert exporter._output[2] == '}'
        
def test_export_function_with_one_function():
        exporter = yaml2cppexporter(statechart2())
        exporter.export_function()
        assert len(exporter._output)/3 == 2 # the show function and the my_function()

        assert exporter._output[0] == 'void ' + 'my_function()'
        assert exporter._output[1] == '{'
        assert exporter._output[2] == '}'

def test_export_function_with_one_function_twice():
        exporter = yaml2cppexporter(statechart3())
        exporter.export_function()
        assert len(exporter._output)/3 == 2 # the show function and the my_function()

        assert exporter._output[0] == 'void ' + 'my_function()'
        assert exporter._output[1] == '{'
        assert exporter._output[2] == '}'

def test_export_function_with_two_function_on_the_same_action():
        exporter = yaml2cppexporter(statechart4())
        exporter.export_function()
        assert len(exporter._output)/3 == 3 # the show, my_function and my_function2

        
        assert exporter._output[0] == 'void ' + 'my_function()'
        assert exporter._output[1] == '{'
        assert exporter._output[2] == '}'

        assert exporter._output[3] == 'void ' + 'my_function2()'
        assert exporter._output[4] == '{'
        assert exporter._output[5] == '}'


def test_export_function_with_one_function_with_parameters():
        exporter = yaml2cppexporter(statechart5())
        exporter.export_function()
        assert len(exporter._output)/3 == 2 # the show and my_function
        
        assert exporter._output[0] == 'void ' + 'my_function(int arg0,int arg1,int arg2)'
        assert exporter._output[1] == '{'
        assert exporter._output[2] == '}'

