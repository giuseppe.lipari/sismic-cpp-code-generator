import pytest
import cpp
from sismic.io import import_from_yaml

def statechart1():
        yaml = """statechart:
  name: example
  preamble:
    x = DEFAULT_X
  root state:
    name: root
"""
        return yaml

def statechart2():
        yaml = """statechart:
  name: example
  preamble:
    x = DEFAULT_X
  root state:
    name: root
    states:
      - name: s1
        transitions:
        - target : s2
      - name: s2
        transitions:
          - target: s1
            event: This is an event
      - name: s3
        transitions:
          - event: This is an event
"""
        return yaml

def statechart3():
        yaml = """statechart:
  name: example
  preamble:
    x = DEFAULT_X
  root state:
    name: root
    states:
      - name: s1
        transitions:
        - target : s2
          guard: |
            #This is a guard
      - name: s2
        transitions:
          - target: s1
            action: |
             #This is an action
"""
        return yaml



def yaml2cppexporter(yaml):
        return cpp.CppExporter(import_from_yaml(yaml)) 

def test_export_transitions_with_no_transition():
        expected = ""        
        exporter = yaml2cppexporter(statechart1())
        exporter.export_transitions()
        assert len(exporter._output) == 0

def test_export_transitions_with_three_simple_transitions():
        expected_s1 = "Transition s1_s2(s1,s2);"
        expected_s2 = "Transition s2_s1(s2,s1,This is an event);"
        expected_s3 = "Transition s3_s3(s3,s3,This is an event);"
        exporter = yaml2cppexporter(statechart2())
        exporter.export_transitions()
        assert len(exporter._output) == 3

        expected_result = {expected_s1,expected_s2,expected_s3}
        assert expected_s1 in exporter._output
        exporter._output.remove(expected_s1)
        assert expected_s2 in exporter._output
        exporter._output.remove(expected_s2)
        assert expected_s3 in exporter._output
        exporter._output.remove(expected_s3)

        assert len(exporter._output) == 0

def test_export_transitions_with_guards_and_actions():
        expected_s1 = "Transition s1_s2(s1,s2,[](Evt_t evt) { return This is a guard;},empty_action);"
        expected_s2 = "Transition s2_s1(s2,s1,[](Evt_t evt) { return ;},[]() {This is an action;});"
        exporter = yaml2cppexporter(statechart3())
        exporter.export_transitions()
        assert len(exporter._output) == 2

        expected_result = {expected_s1 ,expected_s2}

        assert expected_s1 in exporter._output
        exporter._output.remove(expected_s1)
        assert expected_s2 in exporter._output
        exporter._output.remove(expected_s2)

        assert len(exporter._output) == 0
