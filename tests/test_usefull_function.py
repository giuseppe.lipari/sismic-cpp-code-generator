import pytest
import cpp
from sismic.io import import_from_yaml

def statechart1():
        yaml = """statechart:
  name: example
  root state:
    name: root
"""
        return yaml

def statechart2():
        yaml = """statechart:
  name: example
  preamble: |
    # variable_name = a value
    useless line
    # useless line
  root state:
    name: root
"""
        return yaml

def yaml2cppexporter(yaml):
        return cpp.CppExporter(import_from_yaml(yaml)) 

def test_retrieve_variable_with_no_preamble():
        exporter = yaml2cppexporter(statechart1())
        exporter.retrieve_variable()
        assert len(exporter.variable) == 0

def test_retrieve_variable_with_simple_preamble():
        exporter = yaml2cppexporter(statechart2())
        exporter.retrieve_variable()
        assert len(exporter.variable) == 1
        
        assert "variable_name" in exporter.variable


def test_retrieve_cpp_code_with_empty_str():
        exporter = yaml2cppexporter(statechart1())
        cpp_code = ""
        expected = []
        result = exporter.retrieve_cpp_code(cpp_code)
        assert list(result) == expected

def test_retrieve_cpp_code_with_no_cpp_code():
        exporter = yaml2cppexporter(statechart1())
        cpp_code = "some python code\nmy_variable+=1 #this is a python commentary\n"
        expected = []
        result = exporter.retrieve_cpp_code(cpp_code)
        assert list(filter(None,result)) == expected 
        
def test_retrieve_cpp_code_with_cpp_code():
        exporter = yaml2cppexporter(statechart1())
        cpp_code = "some python code\n#some cpp code\n python code\n#another cpp code"
        expected = ['some cpp code','another cpp code']
        result = exporter.retrieve_cpp_code(cpp_code)
        assert list(filter(None,result)) == expected
        
