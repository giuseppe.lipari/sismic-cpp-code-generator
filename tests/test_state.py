import pytest
import cpp
from sismic.io import import_from_yaml

def statechart1():
        yaml = """statechart:
  name: example
  preamble:
    x = DEFAULT_X
  root state:
    name: root
"""
        return yaml

def statechart2():
        yaml = """statechart:
  name: example
  preamble:
    x = DEFAULT_X
  root state:
    name: root
    states:
      - name: s1
      - name: s2
"""
        return yaml

def statechart3():
        yaml = """statechart:
  name: example
  preamble:
    x = DEFAULT_X
  root state:
    name: root
    states:
      - name: s1
        initial: s1_1
        states:
          - name: s1_1
          - name: s1_2
      - name: s2
        parallel states:
          - name: s2_1
          - name: s2_2
      - name: s3
"""
        return yaml

def statechart4():
        yaml = """statechart:
  name: example
  preamble:
    x = DEFAULT_X
  root state:
    name: root
    states:
      - name: s1
        on entry: |
          #this is a test
      - name: s2
        on exit: |
          #this is a test

"""
        return yaml


@pytest.fixture
def lambda_exporter():
        return lambda yaml : cpp.CppExporter(import_from_yaml(yaml)) 

def export_state(exporter:cpp.CppExporter,sourcename:str):
        exporter.export_state(sourcename)

def export_states(exporter:cpp.CppExporter):
        exporter.export_states()




def test_export_state_with_only_one_state_expect_a_Simplestate_for_root(lambda_exporter):
        expected = 'SimpleState ' + 'root("root")' + ';'
        
        exporter = lambda_exporter(statechart1())
        export_state(exporter,'root')
        
        assert exporter._output[0] == expected
        assert len(exporter._output) == 1

def test_export_state_with_many_states_expect_a_CompositeState_for_root(lambda_exporter):
        expected = 'CompositeState ' + 'root("root")' + ';'
        
        exporter = lambda_exporter(statechart2())
        export_state(exporter,'root')
        assert len(exporter._output) == 1
        
        assert exporter._output[0] == expected

def test_export_state_with_many_states_expect_a_Simplestate_for_s1_and_s2(lambda_exporter):
        expected1 = 'SimpleState ' + 's1("s1")' + ';'
        expected2 = 'SimpleState ' + 's2("s2")' + ';'

        exporter = lambda_exporter(statechart2())
        
        export_state(exporter,'s1')
        assert len(exporter._output) == 1
        export_state(exporter,'s2')
        assert len(exporter._output) == 2
        
        assert exporter._output[0] == expected1
        assert exporter._output[1] == expected2


def test_export_state_with_CompositeState_and_ParallelState(lambda_exporter):
        expected_root = 'CompositeState ' + 'root("root")' + ';'
        expected_s1 = 'CompositeState ' + 's1("s1")' + ';'
        expected_s2 = 'OrthogonalState ' + 's2("s2")' + ';'
        expected_s3 = 'SimpleState ' + 's3("s3")' + ';'
        expected_s1_1 = 'SimpleState ' + 's1_1("s1_1")' + ';'
        expected_s1_2 = 'SimpleState ' + 's1_2("s1_2")' + ';'
        expected_s2_1 = 'SimpleState ' + 's2_1("s2_1")' + ';'
        expected_s2_2 = 'SimpleState ' + 's2_2("s2_2")' + ';'

        exporter = lambda_exporter(statechart3())

        export_state(exporter,'root')
        assert len(exporter._output) == 1
        assert exporter._output[0] == expected_root

        export_state(exporter,'s1')
        assert len(exporter._output) == 2
        assert exporter._output[1] == expected_s1

        export_state(exporter,'s2')
        assert len(exporter._output) == 3
        assert exporter._output[2] == expected_s2

        export_state(exporter,'s3')
        assert len(exporter._output) == 4
        assert exporter._output[3] == expected_s3

        export_state(exporter,'s1_1')
        assert len(exporter._output) == 5
        assert exporter._output[4] == expected_s1_1

        export_state(exporter,'s1_2')
        assert len(exporter._output) == 6
        assert exporter._output[5] == expected_s1_2

        export_state(exporter,'s2_1')
        assert len(exporter._output) == 7
        assert exporter._output[6] == expected_s2_1

        export_state(exporter,'s2_2')
        assert len(exporter._output) == 8
        assert exporter._output[7] == expected_s2_2


def test_export_states_with_only_one_state(lambda_exporter):
        expected = 'SimpleState ' + 'root("root")' + ';'
        
        exporter = lambda_exporter(statechart1())
        export_states(exporter)
        assert len(exporter._output) == 4 # 1 (root state) + 3 ('' output in the function)

        
        assert exporter._output[0] == expected
        assert exporter._output[1] == '' # before the output of add_child
        assert exporter._output[2] == '' # before the output of set_initial
        

def test_export_states_with_three_states(lambda_exporter):
        expected_root = 'CompositeState ' + 'root("root")' + ';'
        expected_s1 = 'SimpleState ' + 's1("s1")' + ';'
        expected_s2 = 'SimpleState ' + 's2("s2")' + ';'
        expected_states = {expected_s1,expected_s2,expected_root}

        expected_child1 = 'root'+ '.add_child('+'s1'+');'
        expected_child2 = 'root'+ '.add_child('+'s2'+');'
        expected_childs = {expected_child1,expected_child2}
        
        exporter = lambda_exporter(statechart2())
        export_states(exporter)
        assert len(exporter._output) == 8 # 3 (states) + 2 (add_child) + 3 ('' output in the function)


        for i in range(3):
                assert exporter._output[i] in expected_states
                expected_states.remove(exporter._output[i])

        assert len(expected_states) == 0

        assert exporter._output[3] == '' # before the add_child


        for i in range(4,6):
                assert exporter._output[i] in expected_childs
                expected_childs.remove(exporter._output[i])

        assert len(expected_childs) == 0
        
        assert exporter._output[6] == '' # before the set_initial

def test_export_states_with_eight_states_and_initial(lambda_exporter):
        expected_root = 'CompositeState ' + 'root("root")' + ';'
        expected_s1 = 'CompositeState ' + 's1("s1")' + ';'
        expected_s2 = 'OrthogonalState ' + 's2("s2")' + ';'
        expected_s3 = 'SimpleState ' + 's3("s3")' + ';'
        expected_s1_1 = 'SimpleState ' + 's1_1("s1_1")' + ';'
        expected_s1_2 = 'SimpleState ' + 's1_2("s1_2")' + ';'
        expected_s2_1 = 'SimpleState ' + 's2_1("s2_1")' + ';'
        expected_s2_2 = 'SimpleState ' + 's2_2("s2_2")' + ';'
        expected_states = {expected_root,expected_s1,expected_s2,expected_s3,expected_s1_1,expected_s1_2,expected_s2_1,expected_s2_2}

        expected_child_s1 = 'root'+ '.add_child('+'s1'+');'
        expected_child_s2 = 'root'+ '.add_child('+'s2'+');'
        expected_child_s3 = 'root'+ '.add_child('+'s3'+');'
        expected_child_s1_1 = 's1'+ '.add_child('+'s1_1'+');'
        expected_child_s1_2 = 's1'+ '.add_child('+'s1_2'+');'
        expected_child_s2_1 = 's2'+ '.add_region('+'s2_1'+');'
        expected_child_s2_2 = 's2'+ '.add_region('+'s2_2'+');'
        expected_childs = {expected_child_s1,expected_child_s2,expected_child_s3,expected_child_s1_1,expected_child_s1_2,expected_child_s2_1,expected_child_s2_2}

        expected_initial = 's1'+'.set_initial(' + 's1_1'+');'

        exporter = lambda_exporter(statechart3())
        export_states(exporter)

        assert len(exporter._output) == 19 # 8 (states : len(expected_states))+ 7 (add_child : len(expected_childs)) +1 (set_initial)+ 3 ('' in function)


        for i in range(8):
                assert exporter._output[i] in expected_states
                expected_states.remove(exporter._output[i])
        
        assert len(expected_states) == 0


        assert exporter._output[8] == ''

        for i in range(9,16):
                assert exporter._output[i] in expected_childs
                expected_childs.remove(exporter._output[i])
        
        assert len(expected_childs) == 0

        assert exporter._output[16] == ''

        assert exporter._output[17] == expected_initial


def test_entry_and_exit_code(lambda_exporter):
        expected_s1 = "SimpleState " + "s1"+'("s1",[]() {this is a test;} , empty_action);'
        expected_s2 = "SimpleState " + "s2"+'("s2",empty_action , []() {this is a test;});'

        exporter = lambda_exporter(statechart4())

        export_state(exporter , 's1')
        assert exporter._output[0] == expected_s1
        export_state(exporter , 's2')
        assert exporter._output[1] == expected_s2
