#!/usr/bin/env python3
import sys
import random
import traceback
from sismic.io import import_from_yaml
from sismic.interpreter import Interpreter
from sismic.helpers import log_trace

def executeWithTry(event,tracelist,interpreter):
    try:
        interpreter.queue(event)
        interpreter.execute()
        return True
    except Exception:
        traceback.print_exc()
        print("\n\n\n L'erreur ci-dessus à été levée lors de l'execution de l'évènement : ",event)
        if ( ask_int_rec("voulez vous plus d'informations sur le déroulement des évènement ? (tapez 1 pour oui, 0 pour non) ")) :
            value = ask_int_rec_intervalle("Combient d'évènement voulez vous récupérer ? tapez 0 pour l'ensemble des évènement ou un nombre entre 1 et 10 pour les derniers : ",0,min(10,len(tracelist)))
            if (value == 0):
                for element in tracelist:
                    printelement(element)
            else:
                for i in range(1,value+1):
                    printelement(tracelist[-i])
        return False

def printelement(element):
    print("===============")
    print("évènement: ", element.event, "\ntransitions: " , element.transitions , "\nétats entrant: ", element.entered_states)
    print("\n\nmacroStep: ",element,"\n")
    
def ask_int_rec(sentence):
    try:
        return int(input(sentence))
    except:
        return ask_int_rec(sentence)


def ask_int_rec_intervalle(sentence,debut,fin):
    value = ask_int_rec(sentence)
    if ( value < debut or value > fin):
        return ask_int_rec_intervalle(sentence,debut,fin)
    return value

def test_final_cond(listevent,cond):
    trace = log_trace(interpreter)
    for event in listevent:
         if (not(executeWithTry(event,trace))):
             return False
    return cond()



def main(argv):
    # Load statechart from yaml file
    machine = import_from_yaml(filepath=argv)

    # Create an interpreter for this statechart
    interpreter = Interpreter(machine)
    
    events = machine.events_for()

    #mycond = lambda : interpreter.context['nCoins'] <= 20
    #print(test_final_cond(events,mycond))

    ma_trace = log_trace(interpreter)

    while (executeWithTry(random.choice(events) ,ma_trace,interpreter)):
        pass
if __name__ == "__main__":
    if(len(sys.argv) == 2):
        main(sys.argv[1])
    else:
        print("Usage error : expected 1 argument, got {}".format(len(sys.argv)-1))
