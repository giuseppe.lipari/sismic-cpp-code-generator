# Generate c++ code from a statechart

## Introduction

The objective of the project is to be able to generate the c++ code of a statechart define in yaml.   
The project use [sismic](https://pypi.org/project/sismic/) in order to get a statechart object from a yaml file.

## Installation

To install the project, you need to:   
   
1: install [sismic](https://pypi.org/project/sismic/) by following this [tutorial](https://sismic.readthedocs.io/en/latest/installation.html)   
2: clone this project   

#### SC MINILIB

SC minilib is available [here](https://gitlab.univ-lille.fr/giuseppe.lipari/sc_minilib).

## Execution

#### Tests

The project have some tests you can run. You first need to install pytest, then run the command bellow.

```bash
py.test
```

#### Executing the project

Execute `cpp.py` with at least one argument corresponding to the yaml file.   
A second argument can be provided, it correspond to the file were the code is written. If the second argument is not provided, then the result will be print.

#### Usage Exemple

```bash
./cpp.py example/hierarchy/hierarchy.yaml 
```

```bash
python3 cpp.py example/hierarchy/hierarchy.yaml 
```

```bash
./cpp.py example/hierarchy/hierarchy.yaml /tmp/result.cpp
```

#### Full example with SC MINILIB

```bash
git clone https://gitlab.univ-lille.fr/giuseppe.lipari/sc_minilib.git # clone sc_minilib
git clone https://gitlab.univ-lille.fr/alexis.finard.etu/pji-sismic # clone pji-sismic
./pji-sismic/cpp.py pji-sismic/example/hierarchy/hierarchy.yaml sc_minilib/examples/hierarchy/hierarchy.cpp #generate hierarchy.cpp from hierarchy.yaml
mkdir sc_minilib/build && cd sc_minilib/build && cmake .. && make # compil all the example in the example folder (including the generate one)
```

## Side note

#### Side note about the yaml representation

The project allow you to put c++ code into the yaml. In order to achieve this, you have to put a `#` on the beginning of the line.   
Warning: The cpp code cannot be retrieve if you use `:`. Use `: |` instead.
##### Exemple:

```python
        yaml = """statechart:
  name: example
  root state:
    name: root
    states:
      - name: s1
        on entry: |
          #this is a cpp code
      - name: s2
        on exit: # This line will be converted to None by sismic

"""
```