Feature: VendingMachine

  Scenario: put a coin in the vendingMachine
    When I send event cCoin with nCoins=0
    Then variable nCoins equals 1

  Scenario: VendingMachine is entering CoffeRequest
    When I send event cCoffee with rest=2
    Then state CoffeeRequest is entered
    #And state CupPrepared is entered
    #And state Idle is entered
    #And variable ncoffees equals 1